
<!-- .slide: data-background="./static/images/fiber_titlebg.png" -->

# Numerical Tutorial 1 - Scalar diffraction I

MCC046 Photonics and Lasers

---

# Outline

### Free-space propagation

  * Linear system
  * Fresnel approximation

----

# Outline

### Numerical implementation

  * Impulse response
  * Huygens-Fresnel method
  * Numerical caveats
    * FFT-shift
    * sampling

----

# Outline

### Two-step method (HA1)
  * "Dummy" plane
  * Sampling

---

## Free-space propagation along arbitrary z-axis

<div class="flex justify-center">


<img src="./images/freespacepropagation.png" class="h-80"/>

</div>

### Problem 

<div class="text-3xl">

* Calculate field $g(x,y)=U(x,y,z=d)$ at position $d$ from field $f(x,y)=U(x,y,z=0)$
at zero position. 
* $U(x,y,z)$ is scalar monochromatic 
* Optical system is a  _homogeneous_ medium.

</div>

----

## Fourier optics

An arbitrary wave in free-space can be analysed as a superposition of plane waves

<div class="grid grid-cols-2 content-center">
<img src="./images/plane_waves.png" class="h-80"/>

<div class="grid grid-cols-1 content-center">

<div>

#### Spatial frequencies

$$ \theta_{x,y} = \arcsin(\lambda \nu_{x,y}) $$
$$ \nu_{x,y} = k_{x,y}/2\pi $$

</div>
</div>
</div>

----

## Fresnel approximation

Propagation is largely paraxial

<div class="flex justify-center">
<img src="./images/paraxial.svg" class="h-80"/>
</div>

Therefore:

$$\theta_{x,y} \ll \pi,\ \theta_{x,y} \approx \lambda \nu_{x,y}$$ 

----

## Propagation

<div class="flex justify-center">
<img src="./images/linear_system.png" class="h-60"/>
</div>

Diffraction can be considered a linear system. So we can calculate propagation
as:

$$ U(x,y,d) = U(x,y,0)\otimes h(x,y) $$ where $h(x,y)  = \frac{e^{jkz}}{j\lambda d} \exp\left(\frac{jk(x^2+y^2)}{2 d}\right)$
is the impulse function of diffraction. 

----

## Propagation 2

Therefore we can simulate diffraction as:

$$ U(x,y,d) = \mathcal{F}^{-1}\left[ \mathcal{F}\left[U(x,y,0)\right]\cdot \mathcal{F}\left[h(x,y)\right] \right] $$

* Approach requires 3 Fourier transforms
* Input and output size are the same

**We will follow a different approach.**

----

## Propagation 

We can write out the 2D convolution as

<div class="text-xl">
\begin{eqnarray} 
U(x',y',0)\otimes h(x,y) &=& \int_{-\infty}\int^{\infty} U(x',y',0) h(x-x', y-y') dx'dy' \\\\
 & =& \frac{e^{jkd}}{j\lambda d}\int_{-\infty}\int^{\infty} U(x',y',0) \exp\left(jk\frac{(x-x')^2+(y-y')^2}{2d}\right) dx'dy'
\end{eqnarray}

</div>

----

If we rewrite the above we get:

<div class="text-xl">
\begin{eqnarray} 
U(x,y,d)& =& \frac{e^{jkd}}{j\lambda d}\exp\left(jk\frac{x^2+y^2}{2d}\right)
        & & \cdot \int_{-\infty}\int^{\infty} U(x',y',0) 
        \exp\left(jk\frac{x'^2+y'^2}{2d}\right) 
        \exp\left(-jk\frac{xx' + yy'}{d}\right) dx'dy'
\end{eqnarray} 

With $ \nu_{x,y} = \theta_{x,y}/\lambda =\frac{x,y}{\lambda d}$ we get:

<div >
<!-- .element: class="fragment" data-fragment-index="1" -->
\begin{eqnarray} 
U(x,y,d)& =& \frac{e^{jkd}}{j\lambda d}\exp\left(jk\frac{x^2+y^2}{2d}\right)
        & & \cdot \int_{-\infty}\int^{\infty} U(x',y',0) 
        \exp\left(jk\frac{x'^2+y'^2}{2d}\right) 
        \exp\left(-j\left(2\pi \nu_x x' + 2\pi \nu_y y'\right)\right) dx'dy'
\end{eqnarray} 
<div>
<!-- .element: class="fragment" data-fragment-index="2" -->
<img src="./images/FTbox.svg", class="fixed top-80 -right-60"/>
</div>
</div>

<div>
<!-- .element: class="fragment" data-fragment-index="3" -->
Therefore: 

$$U(x,y,d) = \frac{e^{jkd}}{j\lambda d}\exp\left(jk\frac{x^2+y^2}{2d}\right) \cdot 
\mathcal{F}\left[U(x',y',0) \exp\left(jk \frac{x'^2+y'^2}{2d}\right)\right]$$
</div>
</div>

----

## Huygens-Fresnel principle

<div class="flex justify-center">
<img src="./images/hfm.png" class="h-80"/>
</div>


* often called the Huygens-Fresnel method
* is consistent with Huygens-Fresnel-principle 
* every point in output plane is a superposition of spherical (paraboloidal) waves
 originating from points in object plane.

---

## On numerical implementation

* Fields and positions are discritised
* $x,x',y,y', U(x',y',0), U(x,y,d)$ are all $N \times N$ matrices (arrays)
* use 2D FFT to implement Fourier transform

$$U(x_i,y_i,d) = \frac{e^{jkd}}{j\lambda d}\exp\left(jk\frac{x_i^2+y_i^2}{2d} \right) \cdot 
\mathcal{FFT}\left[ U(x_i',y_i',0) \exp\left(jk \frac{x_i'^2+y_i'^2}{2d}\right)\right](\delta x')^2$$
*Note the scaling term $(\delta')^2$*


----

## About the FFT 

The Fast Fourier Transform algorithm is an extremely efficient way to calculate 
the Fourier Transform, but there are some important caveats

1. Typical FFT implementations expect frequency/time/space to start at 0 and outputs 
start with the 0-frequency component
<div class="grid grid-cols-2"/>
<div>
<img src="./images/fftshift.svg" class="h-40"/>
</div>
<div>
<!-- .element: class="fragment" data-fragment-index="1" -->
<img src="./images/gauss.svg" class="h-40"/>
</div>
</div>

* Use `ifftshift(fft(fftshift(f(x,y))))`
  * `fftshift` shifts such that 0-frequency is at beginning
  * `ifftshift` shifts to 0-frequency centred
  * `fftshift=fftshift` if $N$ is even

----

## About the FFT 

The Fast Fourier Transform algorithm is an extremely efficient way to calculate 
the Fourier Transform, but there are some important caveats

2. The discretisation in the frequency and time domains are coupled
  * $\delta \nu_x = \frac{1}{N\delta x}$ 

#### Important consequence:

- For HFM method from $\nu_x =  \frac{x}{\lambda d}$ 
it follows that $\delta x = \frac{\lambda d}{N \delta x'}$ ($x$ is at output plane
$x'$ is at input plane)⇨ the spatial
extend of the output window can not be chosen freely. 

- Small distance not possible, numerical window is too small

---

## Two-step method

- Variant of the fractional Fourier transform method.
- propagate to a dummy plane ⇨ can choose arbitrary output window

<div class="flex content-center">
<img src="./images/tsm.png" class="h-60"/>
</div>

- Get dummy plane $U_d(\zeta, \eta, L_1)$ from image and object planes via HFM

\begin{eqnarray}
U_d(\zeta_i,\eta_i, L_1) &=& \frac{e^{jk L_1}}{j\lambda L_1}\exp\left(jk\frac{\zeta_i^2+\eta_i^2}{2L_1} \right) \cdot 
\mathcal{FFT}\left[ U(x_i',y_i',0) \exp\left(jk \frac{x_i'^2+y_i'^2}{2L_1}\right)\right](\delta x')^2 \\\\
 &=& \frac{e^{jk L_2}}{j\lambda L_2}\exp\left(jk\frac{\zeta_i^2+\eta_i^2}{2L_2} \right) \cdot 
\mathcal{FFT}\left[ U(x_i,y_i,L) \exp\left(jk \frac{x_i^2+y_i^2}{2L_2}\right)\right](\delta x)^2 
\end{eqnarray}

----

## Two-step method

Together that gives:

<div class="text-lg">
\begin{eqnarray}
\mathcal{FFT}\left[ U(x_i,y_i,L) \exp\left(jk \frac{x_i^2+y_i^2}{2L_2}\right)\right] &=& \frac{(\delta x)^2 }{(\delta x')^2}
\cdot \frac{L_2}{L_1} \cdot \exp\left(jk(L1-L2)\right) \\\\
 & & \cdot \exp\left( jk \frac{\zeta^2+\eta^2}{2}\left( \frac{1}{L_1}-\frac{1}{L_2} \right)\right) \\\\
 & & \cdot \mathcal{FFT}\left[ U(x_i',y_i',0) \exp\left(jk \frac{x_i'^2+y_i'^2}{2L_1}\right)\right]
\end{eqnarray}
</div>

----

## Two-step method

And taking the IFFT on both sides:

<div class="text-lg">
\begin{eqnarray}
U(x_i,y_i,L) &=&  \frac{(\delta x)^2 }{(\delta x')^2}
\cdot \frac{L_2}{L_1} \cdot \exp\left(jk(L1-L2)\right) \cdot \exp\left( jk \frac{x_i^2+y_i^2}{2L_2} \right) \\\\
 & & \cdot \mathcal{IFFT}\left[\exp\left( jk \frac{\zeta^2+\eta^2}{2}\left( \frac{1}{L_1}-\frac{1}{L_2} \right)\right)\right. \\\\
 & & \left. \cdot \mathcal{FFT}\left[ U(x_i',y_i',0) \exp\left(jk \frac{x_i'^2+y_i'^2}{2L_1}\right)\right] \right]
\end{eqnarray}
</div>


----


## Two-step method

The sampling step-size $\delta x_d$ in the dummy plane must be the same from 
output and input plane. Thus:

$$\delta x_{d,in} = \frac{\lambda}{N \delta x'} \cdot L_1 = \delta x_{d, out} = \frac{\lambda}{N\delta x}$$

Therefore we can vary $\delta x$ in the output plane as:

$$ \delta x = \delta x' \frac{L_2}{L_1}$$

and if $\delta x, \delta x'$ and $L$ are specified:

$$b=\delta x' \frac{L_2}{L_1},\ L_1 = L+L_2$$

---

# Numerical Tutorials -- Introduction

<div class="text-xl">

* *5 Lectures*: Jochen Schröder 
* *Tutors (HA support)*: Ali Mirani, Yan Gao

* *HA work:*

  * 4 HAs 
  * Hand in via Canvas 
    * short report (pdf) + code 
  * Deadline: Always before next NT 
  * Pass on all four HAs is required for exam 
  * Pass/fail condition: 
  ```
  if (first submission) > 75% correct:
      pass 
  else if (first submission) < 75% correct:
        second submission required
        if (second submission) > 75% correct:
            pass
        else:
            fail
  else:
      fail
  ```












